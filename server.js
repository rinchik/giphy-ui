const express = require('express');
const path = require('path');
const webpack = require('webpack');
const morgan = require('morgan');
const axios = require('axios');

const app = express();
const appPort = 5050;

const GIPHY_END_POINT = 'https://api.giphy.com/v1/gifs/search?api_key=GZKGwdu6xlIM0iV58yFKJOFLqj0NLXFw&q=';

let FAVORITE_GIFS = [];

let LOADED_GIF_SET = [];

app.use(morgan('combined'));


app.use('/css', express.static(path.resolve('dist/styles')));
app.use('/js', express.static(path.resolve('dist/scripts')));
app.use('/fonts', express.static(path.resolve('dist/fonts')));


app.get('/', (req, res) => {
    res.sendFile(path.resolve('index.html'));
});


app.get('/search/:term', (req, res) => {
    return axios.get(GIPHY_END_POINT + req.params.term)
        .then(result => res.json(transformResponse(result.data)))

});

app.get('/favorites', (req, res) => {
    return res.json(FAVORITE_GIFS);

});

app.get('/toggle-favorite/:id', (req, res) => {

    const favoriteGif = LOADED_GIF_SET.find(gifData => gifData.gifId == req.params.id);
    const favoriteGifExists = FAVORITE_GIFS.find(gifData => gifData.gifId == req.params.id);

    if (favoriteGifExists) {
        FAVORITE_GIFS = FAVORITE_GIFS.filter(gifData => gifData.gifId != req.params.id);
        return res.json({});
    }

    favoriteGif.isFavorite = true;
    FAVORITE_GIFS.push(favoriteGif);
    return res.json({});
});

app.listen(appPort, error => {
    if (error) throw error;
    console.log('Express server is listening on port', appPort);
});


function transformResponse(giphyResponse) {

    LOADED_GIF_SET = giphyResponse.data.map(gifData => {
        return {
            gifUrl: gifData.images.fixed_width.url,
            gifId: gifData.id,
            isFavorite: !!FAVORITE_GIFS.find(favoriteGif => favoriteGif.gifId == gifData.id)
        }
    });

    return LOADED_GIF_SET;
}