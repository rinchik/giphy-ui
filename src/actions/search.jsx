import axios from 'axios';

const searchActions = {
    search(term, dispatch) {
        return axios.get('/search/' + term)
            .then((result) => {
                dispatch({type: 'SEARCH', state: result.data});
            });
    },
    favorites(dispatch) {
        return axios.get('/favorites')
            .then((result) => {
                dispatch({type: 'FAVORITES', state: result.data});
            });
    }

};

export default searchActions;