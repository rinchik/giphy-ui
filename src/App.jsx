import React, {Component} from 'react';
import PropTypes from 'prop-types';

import GifTile from './components/gif-tile';

import './App.pcss';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            gifs: [],
            loading: false,
            showFavorites: false
        };

        this.searchTerm = '';
    }

    componentWillReceiveProps(nextProps) {
        return this.setState({
            gifs: nextProps.gifs,
            loading: false
        });
    }

    searchInputChange() {
        let timeout;

        return (event) => {
            this.searchTerm = event.target.value;
            clearTimeout(timeout);
            timeout = setTimeout(() => {
                this.searchSubmit();
            }, 1000);
        }

    }

    searchSubmit(event) {
        this.setState({
            gifs: [],
            loading: true
        });

        if (event)
            event.preventDefault();

        if (this.searchTerm)
            this.props.search(this.searchTerm);
    }

    renderGifs() {
        return this.state.gifs.map((gifInfo, index) => {
            return <GifTile {...gifInfo} key={index}/>
        });
    }

    getLoader() {
        if (this.state.loading)
            return (
                <div className="loader-container">
                    <div className="loader"/>
                </div>
            );
    }

    toggleFavorites() {
        return (event) => {
            event.preventDefault();

            this.setState({
                gifs: [],
                loading: true
            });

            this.props.favorites();
        };
    }

    render() {
        return (
            <div className="giphy-ui">
                <form className="search-form" onSubmit={this.searchSubmit.bind(this)}>
                    <a href="#" className="favorites-toggle" onClick={this.toggleFavorites()}>
                        <i className="fa fa-star"/> Favorites
                    </a>
                    <input type="text" className="search" onChange={this.searchInputChange()} placeholder="Search"/>
                    <input type="submit" className="submit-button"/>
                </form>
                <div className="search-result">
                    {this.getLoader()}
                    {this.renderGifs()}
                </div>
            </div>
        );
    }
}

App.propTypes = {
    search: PropTypes.func,
    favorites: PropTypes.func,
    gifs: PropTypes.array
};

export default App


