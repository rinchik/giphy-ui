# Giphy Alternative UI

### Run the app

```bash
npm install
```

and 

```bash
npm start
```

Application runs on port `:5050`.

### UI 

Search text-box listens for the input and executes search exactly 1 second after user stopped typing.

_UX can be refined, for performance and UX improvement it might make sense to query search suggestions instead_


### Stack:

* React
* Redux
* CSSnext
* Express


### Testing

* Basic UI snapshot test with JEST

```bash
npm test
```


### Flow 

UI is purely data-driven and only renders data that it gets from the server. 

This is also the case with `Favorites` feature. Application itself doesn't store any data or performs data manipulations.

All data filtering and storage happens on the server side (`/server.js`).

When user adds a gif to his favorites (by clicking on the star icon) fire-and-forget request to the server is executed, 
which adds this gif to the list of favorites that is currently being stored in the server's internal state memory.

This initial functionality explains why all users of this app will have shared Favorites.

With this approach it will be easier to introduce persistent data-store with session management in the future.


### Demo

![Giphy UI Demo](https://gitlab.com/rinchik/giphy-ui/raw/master/giphyui.gif)
 






