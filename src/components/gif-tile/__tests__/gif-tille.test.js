import React from 'react';
import GifTile from '../index';
import renderer from 'react-test-renderer';

const gifMock = {
    gifId: 'testGifId',
    gifUrl: 'http://localhost:5050/',
    isFavorite: false
}

describe('Gif Tiles', () => {
    const tileRenderer = renderer.create(<GifTile {...gifMock} />);
    const tileInstance = tileRenderer.root;

    it('Should render correctly', () => {
        expect(tileRenderer.toJSON()).toMatchSnapshot();
    });

    it('Should render Gif with set URL', () => {
        expect(tileInstance.findByType('img').props.src).toBe(gifMock.gifUrl);
    });
});

