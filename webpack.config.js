const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    devtool: 'source-map',
    target: 'web',
    mode: 'development',
    entry: [
        './client.jsx'
    ],
    output: {
        path: require('path').resolve('dist'),
        filename: './scripts/bundle.js',
        publicPath: '/'
    },
    plugins: [
        new webpack.optimize.OccurrenceOrderPlugin(),
        new ExtractTextPlugin('./styles/style.css', {
            allChunks: true
        }),
    ],
    module: {
        rules: [
            {
                test: /\.(jsx|es6)/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['react', 'env']
                }
            },
            {
                test: /\.pcss$/,
                use: ExtractTextPlugin.extract({
                    use: [
                        {
                            loader: 'css-loader',
                            options: { importLoaders: 1 },
                        },
                        'postcss-loader',
                    ],
                }),
            }
        ]
    },
    resolve: {
        extensions: ['*', '.js', '.jsx', '.css', '.pcss']
    },
    stats: {
        colors: true,
        reasons: true
    },
}
