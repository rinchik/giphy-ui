import React from 'react';
import { render } from 'react-dom';
import App from './src/App'
import { createStore } from 'redux';
import reducer from './src/reducers/search';
import searchActions from './src/actions/search'
import { Provider, connect } from 'react-redux';


const Application = connect(
    function mapStateToProps(state) {
        return {
            gifs: state.gifs,
        };
    },
    function mapDispatchToProps(dispatch) {
        return {
            search: (term) => {
                searchActions.search(term, dispatch);
            },
            favorites: () => {
                searchActions.favorites(dispatch);
            }
        };
    }
)(App);


const store = createStore(reducer);


render(
    <Provider store={store} >
        <Application/>
    </Provider>,
    document.getElementById('app')
)
