import { assign } from 'lodash';

const initialState = {};

export default function(state = initialState, action = {}) {
    switch(action.type) {
        case 'SEARCH':
            return state = assign({}, state, { gifs: action.state });

        case 'FAVORITES':
            return state = assign({}, state, { gifs: action.state });
    }

    return state;
}