import React, {Component} from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';

import './index.pcss';

class GifTile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isFavorite: props.isFavorite
        }
    }

    getStar() {
        let classes = "fa fa-star favorite-star";

        if (this.state.isFavorite)
            classes += " favorite-star__selected"

        return <i className={classes} onClick={this.toggleFavorite.bind(this)}/>;
    }

    toggleFavorite() {
        axios.get('/toggle-favorite/' + this.props.gifId);

        this.setState({
            isFavorite: !this.state.isFavorite
        })
    }


    render() {
        return (
            <div className="gif-tile">
                {this.getStar()}
                <img src={this.props.gifUrl} alt="Click to add to favorite"/>
            </div>
        );
    }
}

GifTile.propTypes = {
    gifUrl: PropTypes.string,
    gifId: PropTypes.string,
    isFavorite: PropTypes.bool
};

export default GifTile


