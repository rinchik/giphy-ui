import React from 'react';
import App from '../App';
import renderer from 'react-test-renderer';

const appMock = {
    gifs: [],
    search: () => {},
    favorites: () => {}
}

describe('Giphy Search app', () => {
    const appRenderer = renderer.create(<App {...appMock}/>);

    it('Should render correctly', () => {
        expect(appRenderer.toJSON()).toMatchSnapshot();
    });

});

